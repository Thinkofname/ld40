#!/usr/bin/env bash
export EMCC_CFLAGS="-g4"
cargo build --target=wasm32-unknown-emscripten
rm -rf out/*
mkdir out
cp index.html out/index.html
cp -rf assets out/assets
cp target/wasm32-unknown-emscripten/debug/*.* out/
cp target/wasm32-unknown-emscripten/debug/deps/ld40-*.* out/
rm out/ld40-*.js

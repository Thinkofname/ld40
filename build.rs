#[macro_use]
extern crate serde_derive;
extern crate serde_json;
extern crate phf_codegen;

use std::fs;
use std::env;
use std::io::Write;

#[derive(Deserialize)]
struct AsepriteInfo {
    frames: Vec<Frame>,
    meta: Meta,
}

#[derive(Deserialize)]
struct Frame {
    frame: Location,
    duration: i32,
}

#[derive(Deserialize)]
struct Location {
    x: i32,
    y: i32,
    w: i32,
    h: i32,
}

#[derive(Deserialize)]
struct Meta {
    #[serde(rename = "frameTags")]
    frame_tags: Vec<FrameTag>,
}

#[derive(Deserialize)]
struct FrameTag {
    name: String,
    from: usize,
    to: usize,
    direction: String,
}

fn main() {
    let mut out = fs::File::create(&format!("{}/animation_info.rs", env::var("OUT_DIR").unwrap()))
        .unwrap();
    for file in fs::read_dir("./assets").unwrap() {
        let mut file = file.unwrap();
        let name = file.file_name();
        let name = name.to_string_lossy();
        if name.ends_with(".json") {
            let f = fs::File::open(file.path()).unwrap();
            let info: AsepriteInfo = serde_json::from_reader(f).unwrap();
            writeln!(out, "pub const ANI_INFO_{}: AnimationInfo = AnimationInfo {{", name[..name.len() - 5].to_uppercase()).unwrap();
            writeln!(out, "    frames: &[").unwrap();
            for frame in &info.frames {
                writeln!(out, "        Frame {{ x: {}, y: {}, w: {}, h: {}, duration: {}f32 }},",
                    frame.frame.x,
                    frame.frame.y,
                    frame.frame.w,
                    frame.frame.h,
                    frame.duration as f32 / (1000.0/60.0),
                ).unwrap();
            }
            writeln!(out, "    ],").unwrap();
            let mut map = phf_codegen::Map::new();
            for tag in info.meta.frame_tags {
                let duration: i32 = info.frames[tag.from .. tag.to + 1]
                    .iter()
                    .map(|v| v.duration)
                    .sum();
                map.entry(tag.name, &format!("Animation {{ from: {}, to: {}, duration: {}f32 }}",
                                             tag.from, tag.to,
                                             duration as f32 / (1000.0/60.0)
                ));
            }
            writeln!(out, "    animations: ").unwrap();
            map.build(&mut out).unwrap();
            writeln!(out, "}};").unwrap();
        }

    }
}
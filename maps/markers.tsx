<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Markers" tilewidth="16" tileheight="16" tilecount="64" columns="8">
 <image source="../assets/markers.png" width="128" height="128"/>
 <tile id="0">
  <properties>
   <property name="solid" type="bool" value="true"/>
  </properties>
 </tile>
</tileset>

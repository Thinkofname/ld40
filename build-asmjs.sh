#!/usr/bin/env bash
export EMCC_CFLAGS="-g4"
cargo build --target=asmjs-unknown-emscripten
rm -rf out/*
mkdir out
cp index-asmjs.html out/index.html
cp -rf assets out/assets
cp target/asmjs-unknown-emscripten/debug/*.* out/
cp target/asmjs-unknown-emscripten/debug/deps/ld40-*.* out/
rm out/ld40-*.js
#![feature(conservative_impl_trait, link_args)]
#![recursion_limit="500"]

extern crate libc;
extern crate rand;
extern crate specs;
#[macro_use]
extern crate stdweb;
#[macro_use]
extern crate serde_derive;
extern crate tiled;
extern crate phf;

pub mod js;
pub mod util;
pub mod map;
pub mod entity;

use std::time;
use std::sync::{Arc, Mutex};
use std::collections::{HashMap, HashSet};
use js::*;
use stdweb::web::document;
use stdweb::unstable::{TryInto, TryFrom};

use specs::{DispatcherBuilder, World, Join};

pub struct Delta(f32);

#[derive(Deserialize)]
struct BodyClientSize {
    width: i32,
    height: i32,
}
js_deserializable!(BodyClientSize);

fn main() {
    stdweb::initialize();
    ::std::panic::take_hook(); // I have a better panic handler than stdweb

    let BodyClientSize{mut width, mut height} = js!(
        return {
            width: document.documentElement.clientWidth,
            height: document.documentElement.clientHeight,
        };
    ).try_into().unwrap();

    let canvas = document().get_element_by_id("main")
        .and_then(|v| Canvas::from_ref(v))
        .expect("Failed to get main canvas");
    let ctx = canvas.get_context_2d();

    canvas.set_width(width);
    canvas.set_height(height);
    ctx.set_image_smoothing(false);

    ctx.set_fill_style("#000000");
    ctx.fill_rect(0, 0, width, height);

    // The game renders to a smaller canvas which is then
    // upscaled
    let game_canvas = Canvas::new();
    game_canvas.set_width(320);
    game_canvas.set_height(240);
    let gctx = game_canvas.get_context_2d();
    gctx.set_image_smoothing(false);

    let mut last_frame = time::Instant::now();
    let mut world = World::new();
    entity::register_components(&mut world);

    let _player = entity::create_player(world.create_entity(), 10.0 * 16.0, 13.0 * 16.0)
        .build();
    let state = Arc::new(Mutex::new(entity::InputState::default()));
    {
        let state = state.clone();
        document().add_event_raw("keydown", move |e: KeyboardEvent| {
            let mut state = state.lock().unwrap();
            match e.key_code() {
                38 => state.up = true,
                40 => state.down = true,
                37 => state.left = true,
                39 => state.right = true,
                90 if state.attack_released => state.attack_pressed = true,
                88 if state.dash_released => state.dash_pressed = true,
                _ => {},
            }
        });
    }
    {
        let state = state.clone();
        document().add_event_raw("keyup", move |e: KeyboardEvent| {
            let mut state = state.lock().unwrap();
            match e.key_code() {
                38 => state.up = false,
                40 => state.down = false,
                37 => state.left = false,
                39 => state.right = false,
                90 => state.attack_released = true,
                88 => state.dash_released = true,
                _ => {},
            }
        });
    }
    let map = {
        let ents = world.read_resource::<specs::EntitiesRes>();
        let updater = world.read_resource::<specs::LazyUpdate>();
        map::Map::load("start", &ents, &updater)
    };
    world.add_resource(Delta(1.0));
    world.add_resource(gctx);
    world.add_resource(map);
    world.add_resource(entity::PlayerInput{state});
    world.add_resource(ActionTracker {
        swap_map: None,
        gems: HashSet::new(),
        fire: HashMap::new(),
    });

    let mut dispatcher = DispatcherBuilder::new()
        .with(entity::TestTriggers, "test_triggers", &[])
        .with(entity::KillLifetime, "kill_lifetime", &[])
        .with(entity::ControlEntity, "control_entity", &[])
        .with(entity::CollideBound, "collide_bound", &[])
        .with(entity::ApplyVelocity, "apply_velocity", &[
            "control_entity",
            "collide_bound",
        ])
        .with(entity::FollowCamera, "follow_camera", &["apply_velocity"])
        .with(entity::DoFloating, "do_floating", &[])
        .with(entity::TickFirePath, "tick_fire_path", &[])
        .with(entity::LaunchArrows,"launch_arrows", &[])
        .with(entity::ArrowTick, "arrow_tick", &[])
        .build();

    let mut draw_dispatcher = DispatcherBuilder::new()
        .with_thread_local(map::DrawMap)
        .with_thread_local(entity::DrawAnimatedSprites {
            loaded_images: Default::default(),
        })
        .with_thread_local(entity::DrawSprites {
            loaded_images: Default::default(),
        })
        .build();

    js::main_loop(move || {
        let start = time::Instant::now();
        let diff = start.duration_since(last_frame);
        last_frame = start;
        let delta =
            (diff.as_secs() * 1_000_000_000 + diff.subsec_nanos() as u64) as f64 / (1_000_000_000.0 / 60.0);
        let delta = delta.min(3.0);
        {
            let mut d = world.write_resource::<Delta>();
            d.0 = delta as f32;
        }

        let BodyClientSize{width: cur_width, height: cur_height} = js!(
            return {
                width: document.documentElement.clientWidth,
                height: document.documentElement.clientHeight,
            };
        ).try_into().unwrap();
        if width != cur_width || height != cur_height {
            width = cur_width;
            height = cur_height;
            canvas.set_width(width);
            canvas.set_height(height);
            ctx.set_image_smoothing(false);
        }

        let can_tick = {
            let ctx = world.read_resource::<Context2d>();
            ctx.set_fill_style("#FF00FF");
            ctx.fill_rect(0, 0, 320, 240);

            world.read_resource::<ActionTracker>().swap_map.is_none()
        };

        if can_tick {
            dispatcher.dispatch(&mut world.res);
        }
        draw_dispatcher.dispatch(&mut world.res);
        world.maintain();

        {
            let ctx = world.read_resource::<Context2d>();
            ctx.restore(); // The map rendering offsets the world
            let mut actions = world.write_resource::<ActionTracker>();
            let mut map = world.write_resource::<map::Map>();
            let ents = world.read_resource::<specs::EntitiesRes>();
            let updater = world.read_resource::<specs::LazyUpdate>();
            if let Some(mut swap) = actions.swap_map.take() {

                swap.fade_time += delta as f32 * 0.05 * swap.fade_speed * if swap.fade_out {
                    1.0
                } else { -1.0 };
                swap.fade_time = swap.fade_time.min(1.0).max(0.0);

                ctx.set_global_alpha(swap.fade_time as f64);
                ctx.set_fill_style("#000000");
                ctx.fill_rect(0, 0, 320, 240);
                ctx.set_global_alpha(1.0);

                if swap.fade_out && swap.fade_time >= 1.0 {
                    swap.fade_out = false;
                    swap.fade_speed = 1.0;
                    {
                        let cons = world.read::<entity::Constant>();
                        let ents = world.entities();
                        for e in ents.join() {
                            if cons.get(e).is_none() {
                                ents.delete(e).unwrap();
                            }
                        }
                    }
                    *map = map::Map::load(&swap.map, &ents, &updater);
                    if let Some(reg) = map.regions.iter()
                        .find(|v| v.name == swap.spawn)
                    {
                        let mut pos = world.write::<entity::Position>();
                        let mut pos = pos.get_mut(swap.entity).unwrap();
                        pos.x = reg.area.0 as f32 + reg.area.2 as f32 * 0.5;
                        pos.y = reg.area.1 as f32 + reg.area.3 as f32 * 0.5;
                    } else {
                        panic!("Missing spawn target: {}", swap.spawn);
                    }
                    actions.swap_map  = Some(swap);
                } else if !swap.fade_out && swap.fade_time <= 0.0 {
                    // Done
                } else {
                    actions.swap_map  = Some(swap);
                }
            }

            actions.fire.retain(|_k, v| ents.is_alive(v.entity));

            for path in &map.paths {
                if let Some(&tiled::PropertyValue::StringValue(ref gem)) = path.properties.get("active_gem") {
                    if actions.gems.contains(gem) && !actions.fire.contains_key(&path.name) {
                        let entity = ents.create();
                        updater.insert(entity, entity::Fire {
                            path: path.points.iter()
                                .map(|v| (path.x + v.0, path.y + v.1))
                                .collect(),
                            progress: (0, 0.0),
                        });
                        actions.fire.insert(path.name.clone(), FireNode {
                            entity,
                        });
                    }
                }
            }
        }

        {
            ctx.set_fill_style("#000000");
            ctx.fill_rect(0, 0, width, height);

            let (dx, dy, dw, dh) = if width > height {
                let dw = (height * 320) / 240;
                (
                    (width-dw) / 2,
                    0,
                    dw,
                    height,
                )
            } else {
                let dh = (width * 240) / 320;
                (
                    0,
                    (height-dh) / 2,
                    width,
                    dh,
                )
            };
            ctx.draw_image_scaled(&game_canvas, dx, dy, dw, dh);
        }
    });
}

pub struct ActionTracker {
    pub swap_map: Option<SwapMap>,
    pub gems: HashSet<String>,
    pub fire: HashMap<String, FireNode>,
}

pub struct FireNode {
    pub entity: specs::Entity,
}

pub struct SwapMap {
    pub map: String,
    pub spawn: String,
    pub entity: specs::Entity,
    pub fade_time: f32,
    pub fade_speed: f32,
    pub fade_out: bool,
}
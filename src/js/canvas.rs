
use stdweb::Reference;
use stdweb::web::document;
use stdweb::web::html_element::ImageElement;
use stdweb::unstable::TryInto;

pub trait Drawable {
    fn can_draw(&self) -> bool {
        true
    }
    fn as_js(&self) -> &Reference;
}

impl Drawable for ImageElement {
    fn can_draw(&self) -> bool {
        self.complete()
    }

    fn as_js(&self) -> &Reference {
        self.as_ref()
    }
}

pub struct Canvas(Reference);

impl Drawable for Canvas {
    fn as_js(&self) -> &Reference {
        &self.0
    }
}

impl Canvas {
    pub fn new() -> Canvas {
        Canvas(
            document().create_element("canvas").into(),
        )

    }

    pub fn from_ref<T>(r: T) -> Option<Canvas>
        where T: Into<Reference>
    {
        let r = r.into();
        let canvas = r.clone();
        let ok: bool = js! {
            return @{r}.tagName.toUpperCase() === "CANVAS";
        }.try_into().ok()?;
        if ok {
            Some(Canvas(canvas))
        } else {
            None
        }
    }

    pub fn set_width(&self, width: i32) {
        js! { @(no_return)
            @{&self.0}.width = @{width};
        };
    }

    pub fn set_height(&self, height: i32) {
        js! { @(no_return)
            @{&self.0}.height = @{height};
        };
    }

    pub fn get_width(&self) -> i32 {
        js! (
            return @{&self.0}.width;
        ).try_into().unwrap()
    }

    pub fn get_height(&self) -> i32 {
        js! (
            return @{&self.0}.height;
        ).try_into().unwrap()
    }

    pub fn get_context_2d(&self) -> Context2d {
        Context2d(js! (
            return @{&self.0}.getContext("2d");
        ).try_into().unwrap())
    }
}

pub struct Context2d(Reference);

impl Context2d {

    pub fn set_image_smoothing(&self, enabled: bool) {
        js! { @(no_return)
            var e = @{enabled};
            var s = @{&self.0};
            s.mozImageSmoothingEnabled = e;
            s.webkitImageSmoothingEnabled = e;
            s.msImageSmoothingEnabled = e;
            s.imageSmoothingEnabled = e;
        }
    }

    pub fn save(&self) {
        js! { @(no_return)
            @{&self.0}.save();
        }
    }

    pub fn restore(&self) {
        js! { @(no_return)
            @{&self.0}.restore();
        }
    }

    pub fn begin_path(&self) {
        js! { @(no_return)
            @{&self.0}.beginPath();
        }
    }

    pub fn clip(&self) {
        js! { @(no_return)
            @{&self.0}.clip();
        }
    }

    pub fn fill(&self) {
        js! { @(no_return)
            @{&self.0}.fill();
        }
    }

    pub fn arc(&self, x: i32, y: i32, radius: i32, start: f64, end: f64) {
        #[derive(Serialize)]
        struct Params {
            x: i32,
            y: i32,
            radius: i32,
            start: f64,
            end: f64,
        }
        js_serializable!( Params );
        let params = Params{x, y, radius, start, end};
        js! { @(no_return)
            var params = @{params};
            @{&self.0}.arc(
                params.x, params.y,
                params.radius, params.start,
                params.end, false
            );
        }
    }

    pub fn set_global_alpha(&self, alpha: f64) {
        js! { @(no_return)
            @{&self.0}.globalAlpha = @{alpha};
        }
    }

    pub fn set_font(&self, font: &str) {
        js! { @(no_return)
            @{&self.0}.font = @{font};
        }
    }

    pub fn set_fill_style<C>(&self, style: &C)
        where C: Color + ?Sized
    {
        style.set_to(&self.0, "fillStyle");
    }

    pub fn translate(&self, x: i32, y: i32) {
        js! { @(no_return)
            @{&self.0}.translate(@{x}, @{y});
        }
    }

    pub fn scale(&self, x: f64, y: f64) {
        js! { @(no_return)
            @{&self.0}.scale(@{x}, @{y});
        }
    }

    pub fn rotate(&self, angle: f64) {
        js! { @(no_return)
            @{&self.0}.rotate(@{angle});
        }
    }

    pub fn fill_rect(&self, x: i32, y: i32, w: i32, h: i32) {
        js! { @(no_return)
            @{&self.0}.fillRect(@{x}, @{y}, @{w}, @{h});
        }
    }

    pub fn draw_image<I: Drawable>(&self, i: &I, dx: i32, dy: i32) {
        if i.can_draw() {
            let r = i.as_js();

            js! { @(no_return)
                @{&self.0}.drawImage(@{r}, @{dx}, @{dy});
            };
        }
    }

    pub fn draw_image_scaled<I: Drawable>(&self, i: &I, dx: i32, dy: i32, dw: i32, dh: i32) {
        if i.can_draw() {
            #[derive(Serialize)]
            struct Params {
                dx: i32,
                dy: i32,
                dw: i32,
                dh: i32,
            }
            js_serializable!( Params );
            let params = Params{dx, dy, dw, dh};
            let r = i.as_js();
            js! { @(no_return)
                var params = @{params};
                @{&self.0}.drawImage(@{r}, params.dx, params.dy, params.dw, params.dh);
            };
        }
    }

    pub fn draw_image_part<I: Drawable>(&self, i: &I, sx: i32, sy: i32, sw: i32, sh: i32, dx: i32, dy: i32, dw: i32, dh: i32) {
        if i.can_draw() {
            #[derive(Serialize)]
            struct Params {
                sx: i32,
                sy: i32,
                sw: i32,
                sh: i32,
                dx: i32,
                dy: i32,
                dw: i32,
                dh: i32,
            }
            js_serializable!( Params );
            let params = Params{sx, sy, sw, sh, dx, dy, dw, dh};
            let r = i.as_js();
            js! { @(no_return)
                var params = @{params};
                @{&self.0}.drawImage(@{r}, params.sx, params.sy, params.sw, params.sh, params.dx, params.dy, params.dw, params.dh);
            };
        }
    }

    pub fn fill_text(&self, text: &str, x: i32, y: i32) {
        js! { @(no_return)
            @{&self.0}.fillText(@{text}, @{x}, @{y});
        }
    }

    pub fn create_radial_gradient(&self, x0: i32, y0: i32, r0: i32, x1: i32, y1: i32, r1: i32) -> CanvasGradient {
        #[derive(Serialize)]
        struct Params {
            x0: i32,
            y0: i32,
            r0: i32,
            x1: i32,
            y1: i32,
            r1: i32,
        }
        js_serializable!( Params );
        let params = Params{x0, y0, r0, x1, y1, r1};
        let r = js! (
            var params = @{params};
            return @{&self.0}.createRadialGradient(
                params.x0, params.y0,
                params.r0,
                params.x1, params.y1,
                params.r1,
            );
        ).try_into().unwrap();
        CanvasGradient(r)
    }
}

pub struct CanvasGradient(Reference);

impl CanvasGradient {
    pub fn add_color_stop(&self, offset: f64, style: &str) {
        js! { @(no_return)
            @{&self.0}.addColorStop(@{offset}, @{style});
        }
    }
}


pub trait Color {
    fn set_to(&self, r: &Reference, key: &str);
}

impl Color for str {
    fn set_to(&self, r: &Reference, key: &str) {
        js! { @(no_return)
            @{r}[@{key}] = @{self};
        }
    }
}

impl Color for String {
    fn set_to(&self, r: &Reference, key: &str) {
        js! { @(no_return)
            @{r}[@{key}] = @{self};
        }
    }
}

impl Color for CanvasGradient {
    fn set_to(&self, r: &Reference, key: &str) {
        js! { @(no_return)
            @{r}[@{key}] = @{&self.0};
        }
    }
}
use libc;

mod canvas;
pub use self::canvas::*;
mod audio;
pub use self::audio::*;
use stdweb::unstable::TryInto;
use stdweb::Reference;

type EMCallbackArg = extern "C" fn(*mut libc::c_void);

#[allow(unused_attributes)]
#[link_args = "--js-library js/lib.js"]
extern "C" {
    fn emscripten_set_main_loop_arg(func: EMCallbackArg, arg: *mut libc::c_void, fps: libc::c_int, simulate_infinite_loop: libc::c_int);
}


pub fn main_loop<F: FnMut() + 'static>(func: F) -> ! {
    unsafe {
        let fpointer = Box::new(func);
        emscripten_set_main_loop_arg(c_main_loop::<F>, Box::into_raw(fpointer) as *mut _, 0, 1);
        unreachable!()
    }
}

extern "C" fn c_main_loop<F: FnMut()>(arg: *mut libc::c_void) {
    let func: *mut F = arg as *mut _;
    unsafe {
        (*func)()
    }
}

pub struct KeyboardEvent(Reference);

impl From<Reference> for KeyboardEvent {
    fn from(v: Reference) -> KeyboardEvent {
        KeyboardEvent(v)
    }
}

impl KeyboardEvent {
    pub fn key_code(&self) -> i32 {
        js!(
            var evt = @{&self.0};
            return evt.which || evt.keyCode || 0;
        ).try_into().unwrap()
    }
}

pub struct MouseEvent(Reference);

impl From<Reference> for MouseEvent {
    fn from(v: Reference) -> MouseEvent {
        MouseEvent(v)
    }
}

impl MouseEvent {
    pub fn button(&self) -> MouseButton {
        let btn: i32 = js!(
            var evt = @{&self.0};
            return evt.button;
        ).try_into().unwrap();
        match btn {
            1 => MouseButton::Middle,
            2 => MouseButton::Right,
            3 => MouseButton::Fourth,
            4 => MouseButton::Fifth,
            _ => MouseButton::Left,
        }
    }

    pub fn client_x(&self) -> i32 {
        js!(
            var evt = @{&self.0};
            return evt.clientX;
        ).try_into().unwrap()
    }

    pub fn client_y(&self) -> i32 {
        js!(
            var evt = @{&self.0};
            return evt.clientY;
        ).try_into().unwrap()
    }
}

#[derive(Debug)]
pub enum MouseButton {
    Left,
    Middle,
    Right,
    Fourth,
    Fifth,
}

pub trait ElementExt {
    fn add_event_raw<F, E>(&self, ty: &str, handler: F)
        where F: FnMut(E) + 'static,
              E: From<Reference>;
}

impl ElementExt for ::stdweb::web::Document
{

    fn add_event_raw<F, E>(&self, ty: &str, mut handler: F)
        where F: FnMut(E) + 'static,
              E: From<Reference>,
    {
        let handler = move |v: Reference| {
            handler(v.into())
        };
        js! { @(no_return)
            var handler = @{handler};
            @{self}.addEventListener(@{ty}, handler);
            // Leak the closure here, fine for now
        }
    }
}

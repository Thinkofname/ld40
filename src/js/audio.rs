
use stdweb::Reference;
use stdweb::unstable::TryInto;

pub struct AudioBuffer(Reference);

impl AudioBuffer {
    pub fn load_async(path: &str) -> AudioBuffer {
        let r = js! (
            var audio_buffer = null;
            audio_buffer = fetch(@{path}).then(function(response) {
                return response.arrayBuffer();
            }).then(function(buf) {
                if (Module.AUDIO_CONTEXT == null) {
                    Module.AUDIO_CONTEXT = new AudioContext();
                }
                return Module.AUDIO_CONTEXT.decodeAudioData(buf, function(buf) {
                    audio_buffer.buffer = buf;
                });
            }).then(function(buf) {
                audio_buffer.buffer = buf;
            });
            return audio_buffer;
        ).try_into().unwrap();

        AudioBuffer(r)
    }
}

pub struct Audio;

impl Audio {
    pub fn play_oneshot(buf: &AudioBuffer, vol: f64, rate: f64) {
        js! { @(no_return)
            var buf = @{&buf.0};
            if (!buf.buffer || !Module.AUDIO_CONTEXT) return;
            var context = Module.AUDIO_CONTEXT;
            var source = context.createBufferSource();
            source.buffer = buf.buffer;
            source.playbackRate.value = @{rate};
            var gain = context.createGain();
            gain.gain.value = @{vol};
            source.connect(gain);
            gain.connect(context.destination);
            source.start(0);
        }
    }
}
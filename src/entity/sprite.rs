use super::*;
use std::collections::HashMap;
use stdweb::web::html_element::ImageElement;

pub struct Sprite {
    pub image: String,
    pub subsection: (i32, i32, i32, i32),
}

impl Component for Sprite {
    type Storage = VecStorage<Self>;
}

pub struct DrawSprites {
    pub loaded_images: HashMap<String, ImageElement>,
}

impl<'a> System<'a> for DrawSprites {
    type SystemData = (
        Fetch<'a, Context2d>,
        WriteStorage<'a, Sprite>,
        ReadStorage<'a, Position>,
    );

    fn run(&mut self, (ctx, mut sprites, pos): Self::SystemData) {
        for (pos, sprite) in (&pos, &mut sprites).join() {
            let sec = sprite.subsection;

            let img = self.loaded_images.entry(sprite.image.clone())
                .or_insert_with(|| {
                    let img = ImageElement::new();
                    img.set_src(&sprite.image);
                    img
                });

            ctx.draw_image_part(
                img,
                sec.0, sec.1, sec.2, sec.3,
                pos.x as i32 - (sec.2 / 2), pos.y as i32 - (sec.3 / 2), sec.2, sec.3,
            );
        }
    }
}
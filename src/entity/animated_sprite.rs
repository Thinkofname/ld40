use super::*;
use phf;
use std::collections::HashMap;
use stdweb::web::html_element::ImageElement;

include!(concat!(env!("OUT_DIR"), "/animation_info.rs"));

pub struct AnimatedSprite {
    pub image: &'static str,
    pub info: &'static AnimationInfo,
    pub animation: &'static str,
    time: f32,
    pub speed: f32,
}

impl AnimatedSprite {
    pub fn new(image: &'static str,
               info: &'static AnimationInfo,
               animation: &'static str) -> AnimatedSprite {
        AnimatedSprite {
            image,
            info,
            animation,
            time: 0.0,
            speed: 1.0,
        }
    }
}

impl Component for AnimatedSprite {
    type Storage = VecStorage<Self>;
}

pub struct DrawAnimatedSprites {
    pub loaded_images: HashMap<&'static str, ImageElement>,
}

impl<'a> System<'a> for DrawAnimatedSprites {
    type SystemData = (
        Fetch<'a, Context2d>,
        Fetch<'a, ::Delta>,
        WriteStorage<'a, AnimatedSprite>,
        ReadStorage<'a, Position>,
    );

    fn run(&mut self, (ctx, delta, mut sprites, pos): Self::SystemData) {
        let delta = delta.0;
        for (pos, sprite) in (&pos, &mut sprites).join() {
            let ani = sprite.info.animations.get(sprite.animation)
                .expect("Missing sprite animation");

            sprite.time += delta * sprite.speed;
            sprite.time = sprite.time % ani.duration;

            if let Some(frame) = sprite.info.frames[ani.from..ani.to + 1]
                .iter()
                .scan(0.0, |state, frame|{
                    *state = *state + frame.duration;
                    if sprite.time < *state {
                        Some(Some(frame))
                    } else {
                        Some(None)
                    }
                })
                .flat_map(|v| v)
                .next()
            {
                let img = self.loaded_images.entry(sprite.image)
                    .or_insert_with(|| {
                        let img = ImageElement::new();
                        img.set_src(&format!("assets/{}.png", sprite.image));
                        img
                    });
                ctx.draw_image_part(
                    img,
                    frame.x, frame.y, frame.w, frame.h,
                    pos.x as i32 - (frame.w/2), pos.y as i32 - (frame.h/2), frame.w, frame.h,
                );
            }
        }
    }
}

pub struct AnimationInfo {
    frames: &'static [Frame],
    animations: phf::Map<&'static str, Animation>,
}

struct Frame {
    x: i32,
    y: i32,
    w: i32,
    h: i32,
    duration: f32,
}

struct Animation {
    from: usize,
    to: usize,
    duration: f32,
}
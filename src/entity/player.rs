
use super::*;
use specs::{EntityBuilder, LazyUpdate, Entities};
use std::sync::{Arc, Mutex};
use rand::{thread_rng, Rng};

pub fn create_player(e: EntityBuilder, x: f32, y: f32) -> EntityBuilder {
    e
        .with(Position { x, y })
        .with( Velocity {
            x: 0.0,
            y: 0.0,
        })
        .with(Bound {
            x: -8.0,
            w: 16.0,
            y: 6.0,
            h: 2.0,
        })
        .with(AnimatedSprite::new("player", &ANI_INFO_PLAYER, "idle_down"))
        .with(Constant)
        .with(CanTrigger)
        .with(CameraFollow)
}
pub struct ControlEntity;

impl<'a> System<'a> for ControlEntity {
    type SystemData = (
        Fetch<'a, ::Delta>,
        Fetch<'a, PlayerInput>,
        ReadStorage<'a, Position>,
        WriteStorage<'a, Velocity>,
        WriteStorage<'a, AnimatedSprite>,
        Entities<'a>,
        Fetch<'a, LazyUpdate>,
    );

    fn run(&mut self, (delta, input, pos, mut vel, mut sprite, ents, updater): Self::SystemData) {
        let delta = delta.0;
        let mut input = input.state.lock().unwrap();
        for (pos, vel, sprite) in (&pos, &mut vel, &mut sprite).join() {
            if input.dash_cooldown <= 0.0 {
                if input.dash_pressed {
                    input.dash_pressed = false;
                    input.dash_time = 20.0;
                    input.dash_cooldown = 100.0;
                    let mut rng = thread_rng();
                    for e in ents.create_iter().take(rng.gen_range(5, 10)) {
                        let mut vel = match input.last {
                            0 => Velocity { x: rng.gen_range(-2.0, 2.0), y: -rng.gen_range(2.0, 4.0) },
                            2 => Velocity { x: rng.gen_range(2.0, 4.0), y: -rng.gen_range(1.0, 3.0) },
                            3 => Velocity { x: -rng.gen_range(2.0, 4.0), y: -rng.gen_range(1.0, 3.0) },
                            _ => Velocity { x: rng.gen_range(-3.0, 3.0), y: -rng.gen_range(0.5, 1.0) },
                        };
                        vel.x *= 0.3;
                        vel.y *= 0.3;
                        updater.insert(e, vel);
                        updater.insert(e, Position {
                            x: pos.x,
                            y: pos.y,
                        });
                        updater.insert(e, Lifetime {
                            life: 30.0,
                        });
                        updater.insert(e, Sprite {
                            image: "assets/general.png".into(),
                            subsection: (16 * rng.gen_range(0, 3), 0, 16, 16),
                        });
                    }
                }
            } else {
                input.dash_pressed = false;
                input.dash_cooldown -= delta;
            }
            let speed = if input.dash_time > 0.0 {
                input.dash_time -= delta;
                sprite.speed = 2.5;
                5.0
            } else {
                sprite.speed = 1.0;
                2.0
            };
            if input.up {
                vel.y = -speed;
                vel.x = 0.0;
                input.last = 0;
                sprite.animation = "walk_up";
            } else if input.down {
                vel.y = speed;
                vel.x = 0.0;
                input.last = 1;
                sprite.animation = "walk_down";
            } else if input.left {
                vel.x = -speed;
                vel.y = 0.0;
                input.last = 2;
                sprite.animation = "walk_left";
            } else if input.right {
                vel.x = speed;
                vel.y = 0.0;
                input.last = 3;
                sprite.animation = "walk_right";
            } else {
                sprite.animation = match input.last {
                    0 => "idle_up",
                    2 => "idle_left",
                    3 => "idle_right",
                    _ => "idle_down",
                };

                vel.y = 0.0;
                vel.x = 0.0;
            }
        }
    }
}

pub struct PlayerInput {
    pub state: Arc<Mutex<InputState>>,
}

#[derive(Debug)]
pub struct InputState {
    pub up: bool,
    pub down: bool,
    pub left: bool,
    pub right: bool,

    pub attack_pressed: bool,
    pub attack_released: bool,
    pub dash_pressed: bool,
    pub dash_released: bool,

    pub last: i32,
    pub dash_time: f32,
    pub dash_cooldown: f32,
}

impl Default for InputState {
    fn default() -> Self {
        InputState {
            up: false,
            down: false,
            left: false,
            right: false,
            attack_pressed: false,
            attack_released: true,
            dash_pressed: false,
            dash_released: true,
            last: 0,
            dash_time: 0.0,
            dash_cooldown: 0.0,
        }
    }
}
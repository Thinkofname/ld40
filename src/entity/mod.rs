mod player;
pub use self::player::*;
mod animated_sprite;
pub use self::animated_sprite::*;
mod sprite;
pub use self::sprite::*;
use tiled;
use specs::{
    Component,
    VecStorage,
    HashMapStorage,
    NullStorage,
    World,
    WriteStorage,
    ReadStorage,
    System,
    Fetch,
    FetchMut,
    Join,
    Entities,
    LazyUpdate,
};
use rand::{thread_rng, Rng};
use js::Context2d;
use map::Map;

pub fn register_components(world: &mut World) {
    world.register::<Position>();
    world.register::<Velocity>();
    world.register::<AnimatedSprite>();
    world.register::<Sprite>();
    world.register::<Bound>();
    world.register::<Lifetime>();
    world.register::<Constant>();
    world.register::<CanTrigger>();
    world.register::<CameraFollow>();
    world.register::<Float>();
    world.register::<Gem>();
    world.register::<Fire>();
    world.register::<ArrowLauncher>();
    world.register::<Arrow>();
}

pub struct CollideBound;

impl<'a> System<'a> for CollideBound {
    type SystemData = (
        Fetch<'a, Map>,
        Fetch<'a, ::Delta>,
        ReadStorage<'a, Position>,
        WriteStorage<'a, Velocity>,
        ReadStorage<'a, Bound>,
    );

    fn run(&mut self, (map, delta, pos, mut vel, bound): Self::SystemData) {
        let delta = delta.0;
        for (pos, vel, bound) in (&pos, &mut vel, &bound).join() {
            // Test X
            {
                if map.test_collision_area(
                    (pos.x + vel.x * delta + bound.x).floor() as i32,
                    (pos.y + bound.y).floor() as i32,
                    bound.w.ceil() as i32,
                    bound.h.ceil() as i32,
                ) {
                    vel.x = 0.0;
                }
            }
            // Test Y
            {
                if map.test_collision_area(
                    (pos.x + bound.x).floor() as i32,
                    (pos.y + vel.y * delta + bound.y).floor() as i32,
                    bound.w.ceil() as i32,
                    bound.h.ceil() as i32,
                ) {
                    vel.y = 0.0;
                }
            }
        }
    }
}

pub struct KillLifetime;

impl<'a> System<'a> for KillLifetime {
    type SystemData = (
        Fetch<'a, ::Delta>,
        Entities<'a>,
        WriteStorage<'a, Lifetime>,
    );

    fn run(&mut self, (delta, ents, mut lifetime): Self::SystemData) {
        let delta = delta.0;
        for (e, life) in (&*ents, &mut lifetime).join() {
            life.life -= delta;
            if life.life <= 0.0 {
                ents.delete(e).unwrap();
            }
        }
    }
}

pub struct ApplyVelocity;

impl<'a> System<'a> for ApplyVelocity {
    type SystemData = (
        Fetch<'a, ::Delta>,
        WriteStorage<'a, Position>,
        WriteStorage<'a, Velocity>,
    );

    fn run(&mut self, (delta, mut pos, mut vel): Self::SystemData) {
        let delta = delta.0;
        for (pos, vel) in (&mut pos, &mut vel).join() {
            pos.x += vel.x * delta;
            pos.y += vel.y * delta;
        }
    }
}

pub struct TestTriggers;

impl<'a> System<'a> for TestTriggers {
    type SystemData = (
        FetchMut<'a, Map>,
        FetchMut<'a, ::ActionTracker>,
        ReadStorage<'a, Position>,
        ReadStorage<'a, Bound>,
        ReadStorage<'a, CanTrigger>,
        Entities<'a>,
        ReadStorage<'a, Gem>,
    );

    fn run(&mut self, (mut map, mut actions, pos, bound, trigger, ents, gem): Self::SystemData) {
        let mut block_areas = vec![];
        for (e, pos, bound, _) in (&*ents, &pos, &bound, &trigger.check()).join() {
            let x = (pos.x + bound.x).floor() as i32;
            let y = (pos.y + bound.y).floor() as i32;
            let w = bound.w.ceil() as i32;
            let h = bound.h.ceil() as i32;

            for reg in &map.regions {
                if !(
                    x + w < reg.area.0
                    || x > reg.area.0 + reg.area.2
                    || y + h < reg.area.1
                    || y > reg.area.1 + reg.area.3
                ) {
                    if let (
                        Some(&tiled::PropertyValue::StringValue(ref target)),
                        Some(&tiled::PropertyValue::StringValue(ref spawn)),
                    ) = (reg.properties.get("target"), reg.properties.get("spawn")) {
                        actions.swap_map = Some(::SwapMap {
                            map: target.clone(),
                            spawn: spawn.clone(),
                            entity: e,
                            fade_time: 0.0,
                            fade_out: true,
                            fade_speed: 1.0,
                        });
                    }
                    if let Some(&tiled::PropertyValue::StringValue(ref kind)) = reg.properties.get("collect_gem") {
                        if !actions.gems.contains(kind) {
                            for (e, gem) in (&*ents, &gem).join() {
                                if gem.kind == *kind {
                                    ents.delete(e).unwrap();
                                }
                            }
                            actions.gems.insert(kind.clone());
                            for reg in &map.regions {
                                if let (
                                    Some(&tiled::PropertyValue::StringValue(ref okind)),
                                    Some(&tiled::PropertyValue::IntValue(tile))
                                )= (reg.properties.get("block_gem"), reg.properties.get("block_path")) {
                                    if *okind == *kind {
                                        block_areas.push((
                                            reg.area,
                                            tile,
                                            reg.properties.get("block_solid") == Some(&tiled::PropertyValue::BoolValue(true))
                                        ));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        let solid = map.get_tile_id(map.marker_layer, 0, 0).unwrap();
        let marker = map.marker_layer;
        for (area, tile, make_solid) in block_areas {
            let min_x = area.0 / 16;
            let min_y = area.1 / 16;
            let max_x = min_x + ((area.2 + 15) / 16);
            let max_y = min_y + ((area.3 + 15) / 16);
            for y in min_y .. max_y {
                for x in min_x .. max_x {
                    map.set_tile_id(marker, x, y, if make_solid {
                        solid
                    } else { 0 });
                    map.set_tile_id(0, x, y, tile as u32);
                }
            }
        }
    }
}

pub struct FollowCamera;

impl<'a> System<'a> for FollowCamera {
    type SystemData = (
        FetchMut<'a, Map>,
        ReadStorage<'a, Position>,
        ReadStorage<'a, CameraFollow>,
    );

    fn run(&mut self, (mut map, pos, follow): Self::SystemData) {
        for (pos, _) in (&pos, &follow.check()).join() {
            let mut cx = pos.x as i32 - 160;
            let mut cy = pos.y as i32 - 120;
            if cx < 0 {
                cx = 0;
            } else if cx + 320 >= map.width as i32 * 16 {
                cx = map.width as i32 * 16 - 320;
            }
            if cy < 0 {
                cy = 0;
            } else if cy + 240 >= map.height as i32 * 16 {
                cy = map.height as i32 * 16 - 240;
            }
            map.draw_offset = (cx, cy);
        }
    }
}

pub struct DoFloating;

impl<'a> System<'a> for DoFloating {
    type SystemData = (
        Fetch<'a, ::Delta>,
        WriteStorage<'a, Position>,
        WriteStorage<'a, Float>,
    );

    fn run(&mut self, (delta, mut pos, mut float): Self::SystemData) {
        use std::f32::consts::PI;
        let delta = delta.0;
        for (pos, float) in (&mut pos, &mut float).join() {
            if float.pos.is_none() {
                float.pos = Some(pos.clone());
            }
            let orig = float.pos.as_ref().unwrap();
            pos.x = orig.x;
            pos.y = orig.y + float.amount * (float.time * PI * 2.0).sin();
            float.time += delta * 0.01;
        }
    }
}

#[derive(Debug, Clone)]
pub struct Position {
    pub x: f32,
    pub y: f32,
}

impl Component for Position {
    type Storage = VecStorage<Self>;
}

#[derive(Debug)]
pub struct Velocity {
    pub x: f32,
    pub y: f32,
}

impl Component for Velocity {
    type Storage = VecStorage<Self>;
}

#[derive(Debug)]
pub struct Bound {
    pub x: f32,
    pub y: f32,
    pub w: f32,
    pub h: f32,
}

impl Component for Bound {
    type Storage = VecStorage<Self>;
}

#[derive(Debug)]
pub struct Float {
    pub amount: f32,
    pub time: f32,
    pub pos: Option<Position>,
}

impl Component for Float {
    type Storage = HashMapStorage<Self>;
}

#[derive(Debug)]
pub struct Gem {
    pub kind: String
}

impl Component for Gem {
    type Storage = HashMapStorage<Self>;
}

pub struct Lifetime {
    pub life: f32,
}

impl Component for Lifetime {
    type Storage = HashMapStorage<Self>;
}

#[derive(Default)]
pub struct Constant;

impl Component for Constant {
    type Storage = NullStorage<Self>;
}

#[derive(Default)]
pub struct CanTrigger;

impl Component for CanTrigger {
    type Storage = NullStorage<Self>;
}

#[derive(Default)]
pub struct CameraFollow;

impl Component for CameraFollow {
    type Storage = NullStorage<Self>;
}

pub struct Fire {
    pub path: Vec<(f32, f32)>,
    pub progress: (usize, f32),
}

impl Component for Fire {
    type Storage = HashMapStorage<Self>;
}

pub struct TickFirePath;

impl<'a> System<'a> for TickFirePath {
    type SystemData = (
        FetchMut<'a, ::ActionTracker>,
        Fetch<'a, ::Delta>,
        WriteStorage<'a, Fire>,
        Entities<'a>,
        Fetch<'a, LazyUpdate>,
        ReadStorage<'a, Position>,
        ReadStorage<'a, CanTrigger>,
    );

    fn run(&mut self, (mut actions, delta, mut fire, ents, updater, pos, trigger): Self::SystemData) {
        let delta = delta.0;
        for fire in (&mut fire).join() {
            let current = if let Some(c) = fire.path.windows(2)
                .nth(fire.progress.0)
            {
                c
            } else {
                continue;
            };

            let dx = current[1].0 - current[0].0;
            let dy = current[1].1 - current[0].1;
            let (dir, nx, ny) = if dx.abs() > dy.abs() {
                if dx > 0.0 {
                    (3, 0.0, 1.0)
                } else {
                    (2, 0.0, 1.0)
                }
            } else {
                if dy > 0.0 {
                    (1, 1.0, 0.0)
                } else {
                    (0, 1.0, 0.0)
                }
            };

            let len = dy.hypot(dx);

            let cx = current[0].0 + (dx / len) * fire.progress.1;
            let cy = current[0].1 + (dy / len) * fire.progress.1;

            let mut rng = thread_rng();
            for e in ents.create_iter().take(rng.gen_range(1, 5)) {
                let mut vel = match dir {
                    0 => Velocity { x: rng.gen_range(-2.0, 2.0), y: -rng.gen_range(2.0, 4.0) },
                    2 => Velocity { x: rng.gen_range(2.0, 4.0), y: -rng.gen_range(1.0, 3.0) },
                    3 => Velocity { x: -rng.gen_range(2.0, 4.0), y: -rng.gen_range(1.0, 3.0) },
                    _ => Velocity { x: rng.gen_range(-3.0, 3.0), y: -rng.gen_range(0.5, 1.0) },
                };
                vel.x *= 0.1;
                vel.y *= 0.1;
                updater.insert(e, vel);
                updater.insert(e, Position {
                    x: cx + rng.gen_range(-16.0 * 4.0, 16.0 * 4.0) * nx,
                    y: cy + rng.gen_range(-16.0 * 4.0, 16.0 * 4.0) * ny,
                });
                updater.insert(e, Lifetime {
                    life: 100.0,
                });
                updater.insert(e, Sprite {
                    image: "assets/general.png".into(),
                    subsection: (3 * 16 + 16 * rng.gen_range(0, 3), 0, 16, 16),
                });
            }

            let min_x = cx - 16.0 * 4.0 * nx - 5.0 * ny;
            let max_x = cx + 16.0 * 4.0 * nx + 5.0 * ny;

            let min_y = cy - 16.0 * 4.0 * ny - 5.0 * nx;
            let max_y = cy + 16.0 * 4.0 * ny + 5.0 * nx;

            for (e, pos, _) in (&*ents, &pos, &trigger.check()).join() {
                if pos.x >= min_x && pos.x <= max_x && pos.y >= min_y && pos.y <= max_y {
                    actions.swap_map = Some(::SwapMap {
                        map: "start".into(),
                        spawn: "start_point".into(),
                        entity: e,
                        fade_time: 0.0,
                        fade_out: true,
                        fade_speed: 0.2,
                    });
                    actions.gems.clear();
                }
            }

            fire.progress.1 += delta * (1.0 + actions.gems.len() as f32 * 0.35);
            if fire.progress.1 >= len {
                fire.progress.0 += 1;
                fire.progress.1 = 0.0;
            }
        }
    }
}

pub struct ArrowLauncher {
    pub speed: f32,
    pub time: f32,
}

impl Component for ArrowLauncher {
    type Storage = HashMapStorage<Self>;
}

pub struct LaunchArrows;

impl<'a> System<'a> for LaunchArrows {
    type SystemData = (
        Fetch<'a, ::Delta>,
        ReadStorage<'a, Position>,
        WriteStorage<'a, ArrowLauncher>,
        Entities<'a>,
        Fetch<'a, LazyUpdate>,
    );

    fn run(&mut self, (delta, pos, mut arrows, ents, updater): Self::SystemData) {
        let delta = delta.0;
        for (pos, arrows) in (&pos, &mut arrows).join() {
            arrows.time += delta;
            if arrows.time >= 100.0 * arrows.speed {
                arrows.time = 0.0;
                let arrow = ents.create();
                updater.insert(arrow, pos.clone());
                updater.insert(arrow, Arrow {
                    start: true,
                });
                updater.insert(arrow, Sprite {
                    image: "assets/general.png".into(),
                    subsection: (0, 16, 16, 16),
                });
            }
        }
    }
}



pub struct Arrow {
    start: bool,
}

impl Component for Arrow {
    type Storage = HashMapStorage<Self>;
}

pub struct ArrowTick;

impl<'a> System<'a> for ArrowTick {
    type SystemData = (
        FetchMut<'a, ::ActionTracker>,
        Fetch<'a, ::Delta>,
        Fetch<'a, Map>,
        WriteStorage<'a, Position>,
        WriteStorage<'a, Arrow>,
        Entities<'a>,
        ReadStorage<'a, CanTrigger>,
    );

    fn run(&mut self, (mut actions, delta, map, mut pos, mut arrow, ents, trigger): Self::SystemData) {
        let delta = delta.0;
        for (e, pos, arrow) in (&*ents, &mut pos, &mut arrow).join() {
            pos.y += delta * 2.5;
            if map.test_collision_area(
                (pos.x - 5.0).floor() as i32,
                (pos.y - 5.0).floor() as i32,
                10, 10
            ) {
                if !arrow.start {
                    ents.delete(e).unwrap();
                }
            } else if arrow.start {
                arrow.start = false;
            }
        }


        for (loc, _) in (&pos, &arrow).join() {
            let min_x = loc.x - 4.0;
            let min_y = loc.y - 4.0;
            let max_x = loc.x + 4.0;
            let max_y = loc.y + 4.0;
            for (e, pos, _) in (&*ents, &pos, &trigger.check()).join() {
                if pos.x >= min_x && pos.x <= max_x && pos.y >= min_y && pos.y <= max_y {
                    actions.swap_map = Some(::SwapMap {
                        map: "start".into(),
                        spawn: "start_point".into(),
                        entity: e,
                        fade_time: 0.0,
                        fade_out: true,
                        fade_speed: 0.2,
                    });
                    actions.gems.clear();
                }
            }
        }
    }
}
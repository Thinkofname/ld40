
use tiled;
use std::io;
use stdweb::web::html_element::ImageElement;
use js::{Canvas, Context2d};

use specs::{System, Fetch, FetchMut, EntitiesRes, LazyUpdate};
use entity;

fn get_map_data(name: &str) -> Option<&'static [u8]> {
    Some(match name {
        "start" => include_bytes!("../maps/start.tmx"),
        "initial_area" => include_bytes!("../maps/initial_area.tmx"),
        "second_area" => include_bytes!("../maps/second_area.tmx"),
        "end" => include_bytes!("../maps/end.tmx"),
        _ => return None,
    })
}

pub struct Map {
    dirty: bool,
    images: Vec<Image>,
    waiting_images: Vec<usize>,
    canvas: Canvas,

    pub width: u32,
    pub height: u32,
    layers: Vec<Layer>,
    tiles: Vec<Option<Tile>>,
    pub regions: Vec<Region>,
    pub paths: Vec<Path>,

    debug_render: bool,
    pub marker_layer: usize,

    pub draw_offset: (i32, i32),
}

pub struct Region {
    pub name: String,
    pub area: (i32, i32, i32, i32),
    pub properties: tiled::Properties,
}

pub struct Path {
    pub name: String,
    pub x: f32,
    pub y: f32,
    pub points: Vec<(f32, f32)>,
    pub properties: tiled::Properties,
}

impl Map {
    pub fn load(name: &str, ents: &EntitiesRes, updater: &LazyUpdate) -> Map {
        let data = get_map_data(name)
            .expect("Missing map data");
        let map = tiled::parse(io::Cursor::new(data)).unwrap();

        let mut tiles: Vec<Option<Tile>> = vec![];

        let mut images: Vec<Image> = vec![];
        let mut waiting_images : Vec<usize> = vec![];

        for ts in map.tilesets {
            for img in ts.images {
                let id = match images.iter().position(|v| v.source == img.source) {
                    Some(pos) => pos,
                    None => {
                        let id = images.len();
                        let element = ImageElement::new();
                        element.set_src(&img.source[3..]);
                        images.push(Image {
                            source: img.source.clone(),
                            element,
                            width: img.width as u32,
                            height: img.height as u32,
                        });
                        waiting_images.push(id);
                        id
                    },
                };
                let i = &images[id];
                let tw = (i.width + (ts.tile_width - 1)) / ts.tile_width;
                let th = (i.height + (ts.tile_height - 1)) / ts.tile_height;
                let len = (tw * th) as usize;
                if ts.first_gid as usize + len >= tiles.len() {
                    tiles.resize(ts.first_gid as usize + len, None);
                }
                for idx in 0..len {
                    tiles[ts.first_gid as usize + idx] = Some(Tile {
                        image: id,
                        ox: ((idx % tw as usize) * 16) as i32,
                        oy: ((idx / tw as usize) * 16) as i32,
                        properties: tiled::Properties::default(),
                    });
                }
            }
            for orig_tile in ts.tiles {
                let idx = (ts.first_gid + orig_tile.id) as usize;
                if idx >= tiles.len() {
                    tiles.resize(idx, None);
                }
                let tile = tiles[idx].as_mut().unwrap();
                tile.properties = orig_tile.properties;
            }
        }

        let mut layers = vec![];
        let mut marker_layer = None;
        for layer in map.layers {
            if layer.name == "Markers" {
                marker_layer = Some(layers.len());
            }
            let mut map_data = vec![0; (map.width * map.height) as usize];
            let width = map.width as usize;
            for (y, row) in layer.tiles.into_iter().enumerate() {
                for (x, tile) in row.into_iter().enumerate() {
                    map_data[x + y * width] = tile;
                }
            }
            layers.push(Layer {
                name: layer.name,
                tiles: map_data,
            });
        }

        let canvas = Canvas::new();
        canvas.set_width((map.width * 16) as i32);
        canvas.set_height((map.height * 16) as i32);

        let mut regions = vec![];
        let mut paths = vec![];
        for group in map.object_groups {
            if group.name == "Regions" {
                for obj in group.objects {
                    match obj.shape {
                        tiled::ObjectShape::Rect {width, height} => {
                            regions.push(Region {
                                name: obj.name,
                                area: (obj.x as i32, obj.y as i32, width as i32, height as i32),
                                properties: obj.properties,
                            });

                        },
                        tiled::ObjectShape::Polyline { points } => {
                            paths.push(Path {
                                name: obj.name,
                                x: obj.x,
                                y: obj.y,
                                points,
                                properties: obj.properties,
                            })
                        },
                        _ => {}
                    }
                }
            } else {
                for (obj, e) in group.objects.into_iter().zip(ents.create_iter()) {
                    updater.insert(e, entity::Position {
                        x: obj.x + 8.0,
                        y: obj.y - 8.0,
                    });
                    let tile = tiles[obj.gid as usize].as_ref().unwrap();
                    let img = &images[tile.image];
                    updater.insert(e, entity::Sprite {
                        image: img.source[3..].to_owned(),
                        subsection: (tile.ox, tile.oy, 16, 16),
                    });
                    if let tiled::ObjectShape::Rect {width, height} = obj.shape {
                        updater.insert(e, entity::Bound {
                            x: -width/2.0,
                            y: -height/2.0,
                            w: width/2.0,
                            h: height/2.0,
                        });
                    }
                    if let Some(&tiled::PropertyValue::FloatValue(float)) = obj.properties.get("float") {
                        updater.insert(e, entity::Float {
                            amount: float,
                            time: 0.0,
                            pos: None,
                        });
                    }
                    if let Some(&tiled::PropertyValue::StringValue(ref kind)) = obj.properties.get("gem") {
                        updater.insert(e, entity::Gem {
                            kind: kind.clone(),
                        });
                    }
                    if let Some(&tiled::PropertyValue::FloatValue(speed)) = obj.properties.get("arrow_launcher") {
                        updater.insert(e, entity::ArrowLauncher {
                            speed,
                            time: 0.0,
                        });
                    }
                }
            }
        }

        Map {
            dirty: true,
            images,
            waiting_images,
            canvas,

            width: map.width,
            height: map.height,
            layers,
            tiles,
            regions,
            paths,

            debug_render: cfg!(debug_assertions),
            marker_layer: marker_layer.expect("Missing markers"),
            draw_offset: (0, 0),
        }
    }

    pub fn get_tile_id(&self, layer: usize, x: i32, y: i32) -> Option<u32> {
        if !self.in_bounds(x, y) {
            return None;
        }
        Some(self.layers[layer]
            .tiles[(x + y * self.width as i32) as usize])
    }

    pub fn set_tile_id(&mut self, layer: usize, x: i32, y: i32, id: u32) {
        if !self.in_bounds(x, y) {
            return;
        }
        let marker = self.marker_layer;
        self.dirty |= layer != marker;
        self.layers[layer]
            .tiles[(x + y * self.width as i32) as usize] = id;
    }

    pub fn get_tile_info(&self, id: u32) -> &Tile {
        self.tiles[id as usize]
            .as_ref()
            .unwrap()
    }

    pub fn get_marker_property(&self, x: i32, y: i32, key: &str) -> Option<&tiled::PropertyValue> {
        if !self.in_bounds(x, y) {
            return None;
        }
        self.tiles[self.layers[self.marker_layer]
            .tiles[(x + y * self.width as i32) as usize] as usize]
            .as_ref()
            .and_then(|v| v.properties.get(key))
    }

    pub fn in_bounds(&self, x: i32, y: i32) -> bool {
        x >= 0 && y >= 0 && x < self.width as i32 && y < self.height as i32
    }

    pub fn test_collision_area(&self, x: i32, y: i32, w: i32, h: i32) -> bool {
        let x = x / 16;
        let y = y / 16;
        let w = w / 16;
        let h = h / 16;
        for y in y .. y + h + 1 {
            for x in x .. x + w + 1 {
                if !self.in_bounds(x, y) {
                    return true;
                }
                if let Some(&tiled::PropertyValue::BoolValue(true)) = self.get_marker_property(x, y, "solid") {
                    return true;
                }
            }
        }
        false
    }

    fn rebuild_map(&mut self) {
        let ctx = self.canvas.get_context_2d();
        ctx.set_fill_style("#000000");
        ctx.fill_rect(0, 0, self.width as i32 * 16, self.height as i32 * 16);

        for layer in &self.layers {
            if !self.debug_render && layer.name == "Markers" {
                continue;
            }
            if layer.name == "Markers" {
                ctx.set_global_alpha(0.4);
            }
            for y in 0 .. self.height as usize {
                for x in 0 .. self.width as usize {
                    let id = layer.tiles[x + y * self.width as usize];
                    let tile = if let Some(t) = self.tiles[id as usize].as_ref() {
                        t
                    } else {
                        continue
                    };
                    let img = &self.images[tile.image];

                    ctx.draw_image_part(
                        &img.element,
                        tile.ox, tile.oy,
                        16, 16,
                        x as i32 * 16, y as i32 * 16,
                        16, 16
                    );
                }
            }
            if layer.name == "Markers" {
                ctx.set_global_alpha(1.0);
            }
        }
        if self.debug_render {
            ctx.set_fill_style("rgba(0, 255, 0, 0.4)");
            for reg in &self.regions {
                ctx.fill_rect(reg.area.0, reg.area.1, reg.area.2, reg.area.3);
            }
        }
    }

    fn draw(&mut self, target: &Context2d) {
        {
            let imgs = &self.images;
            let dirty = &mut self.dirty;
            self.waiting_images.retain(|v| if imgs[*v].element.complete() {
                *dirty = true;
                false
            } else { true });
        }

        if self.dirty {
            self.dirty = false;
            self.rebuild_map();
        }

        target.draw_image_part(&self.canvas,
                               self.draw_offset.0, self.draw_offset.1, 320, 240,
                               0, 0, 320, 240);

        target.save();
        target.translate(-self.draw_offset.0, -self.draw_offset.1);
    }
}


pub struct DrawMap;

impl<'a> System<'a> for DrawMap {
    type SystemData = (
        Fetch<'a, Context2d>,
        FetchMut<'a, Map>,
    );

    fn run(&mut self, (ctx, mut map): Self::SystemData) {
        map.draw(&ctx);
    }
}


struct Layer {
    name: String,
    tiles: Vec<u32>,
}

#[derive(Debug)]
struct Image {
    source: String,
    element: ImageElement,
    width: u32,
    height: u32,
}

#[derive(Clone, Debug)]
pub struct Tile {
    image: usize,
    ox: i32,
    oy: i32,
    pub properties: tiled::Properties,
}
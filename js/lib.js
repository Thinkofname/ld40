

mergeInto(LibraryManager.library, {
    // Hacks to get backtraces into a somewhat useful state
    _Unwind_Backtrace__deps: ['emscripten_get_callstack_js'],
    _Unwind_Backtrace: function(func, arg) {
        Error.stackTraceLimit = Infinity;
        var trace = _emscripten_get_callstack_js();
        var parts = trace.split('\n');
        for (var i = 0; i < parts.length; i++) {
            var line = parts[i];
            var atPos = line.indexOf("at") + 3;
            var desc = line.substring(atPos, line.indexOf("(") - 1);
            if (desc.indexOf("Array.") == 0) {
                desc = desc.substring(6);
            }
            if (desc.indexOf("__ZN") != 0) {
                continue;
            }
            desc = desc.substring(1);
            var ptr = Module._malloc(desc.length + 1);
            Module.stringToUTF8(desc, ptr, desc.length + 1);

            var ret = Runtime.dynCall('iii', func, [ptr, arg]);
            // Module._free(ptr);
            if (ret !== 0) return;
        }
        console.trace();
        return 5;
    },
    _Unwind_FindEnclosingFunction: function(ctx) {
        return ctx;
    },
    _Unwind_GetIPInfo: function(ctx) {
        return ctx;
    },
    dladdr: function(addr, info) {
        var line = UTF8ToString(addr + 1);
        var ptr = Module._malloc(line.length + 1);
        Module.stringToUTF8(line, ptr, line.length + 1);
        Module.setValue(info + 8, ptr, "*");
        return 1;
    },
});
